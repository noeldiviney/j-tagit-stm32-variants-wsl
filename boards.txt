# See: https://github.com/arduino/Arduino/wiki/Arduino-IDE-1.5-3rd-party-Hardware-specification
#  J-Tagit Boards
menu.pnum=Board part number

menu.xserial=U(S)ART support
menu.usb=USB support (if available)
menu.xusb=USB speed (if available)
menu.virtio=Virtual serial support

menu.opt=Optimize
menu.dbg=Debug symbols
menu.rtlib=C Runtime Library
menu.upload_method=Upload method

################################################################################
# J-Tagit F1
F103CBT6_BluePill.name=J-Tagit STM32F1 series

F103CBT6_BluePill.build.board=F103CBT6_BluePill
F103CBT6_BluePill.build.core=arduino
F103CBT6_BluePill.build.mcu=cortex-m3
F103CBT6_BluePill.build.series=STM32F1xx
F103CBT6_BluePill.build.cpu=F103CBT6
F103CBT6_BluePill.build.vendor=Eicon
F103CBT6_BluePill.build.kernel=wsl
F103CBT6_BluePill.build.product_line=STM32F103xB
F103CBT6_BluePill.build.variant_h=variant_F103CBT6.h
F103CBT6_BluePill.build.variant=STM32F1xx/F103CBT6
F103CBT6_BluePill.build.cmsis_lib_gcc=arm_cortexM3l_math
F103CBT6_BluePill.build.extra_flags=-D{build.product_line} {build.enable_usb} {build.xSerial} {build.bootloader_flags}

F103CBT6_BluePill.menu.pnum.1=F103CBT6_BluePill (or C8 with 128k)
F103CBT6_BluePill.menu.pnum.1.upload.maximum_size=131072
F103CBT6_BluePill.menu.pnum.1.upload.maximum_data_size=20480
##F103CBT6_BluePill.menu.pnum.1.build.board=F103CBT6_BluePill
##F103CBT6_BluePill.menu.pnum.1.build.product_line=STM32F103xB
##F103CBT6_BluePill.menu.pnum.1.build.variant_h=variant_F103CBT6.h
##F103CBT6_BluePill.menu.pnum.1.build.variant=STM32F1xx/F103CBT6

##F103CBT6_BluePill.menu.pnum.BLUEPILL_F103CBT6=F103CBT6_BLUEPILL (or C8 with 128k)
##F103CBT6_BluePill.menu.pnum.BLUEPILL_F103CBT6.upload.maximum_size=131072
##F103CBT6_BluePill.menu.pnum.BLUEPILL_F103CBT6.upload.maximum_data_size=20480
##F103CBT6_BluePill.menu.pnum.BLUEPILL_F103CBT6.build.board=F103CBT6_BLUEPILL
##F103CBT6_BluePill.menu.pnum.BLUEPILL_F103CBT6.build.product_line=STM32F103xB
##F103CBT6_BluePill.menu.pnum.BLUEPILL_F103CBT6.build.variant_h=variant_F103CBT6.h
##F103CBT6_BluePill.menu.pnum.BLUEPILL_F103CBT6.build.variant=STM32F1xx/F103CBT6

# Upload menu
F103CBT6_BluePill.menu.upload_method.jtagit-2-swd=openocd jtagit-2 swd
F103CBT6_BluePill.menu.upload_method.jtagit-2-swd.upload.tool=openocd
F103CBT6_BluePill.menu.upload_method.jtagit-2-swd.upload.protocol=swd
F103CBT6_BluePill.menu.upload_method.jtagit-2-swd.upload.probe=jtagit-2

F103CBT6_BluePill.menu.upload_method.jtagit-2-jtag=openocd jtagit-2 jtag
F103CBT6_BluePill.menu.upload_method.jtagit-2-jtag.upload.tool=openocd
F103CBT6_BluePill.menu.upload_method.jtagit-2-jtag.upload.protocol=jtag
F103CBT6_BluePill.menu.upload_method.jtagit-2-jtag.upload.probe=jtagit-2

F103CBT6_BluePill.menu.upload_method.openocd_debug=Jtagit-2 w/openocd+debug
F103CBT6_BluePill.menu.upload_method.openocd_debug.upload.tool=openocd_debug
F103CBT6_BluePill.menu.upload_method.openocd_debug.upload.protocol=0

F103CBT6_BluePill.menu.upload_method.swdMethod=STM32CubeProgrammer (SWD)
F103CBT6_BluePill.menu.upload_method.swdMethod.upload.protocol=0
F103CBT6_BluePill.menu.upload_method.swdMethod.upload.options=-g
F103CBT6_BluePill.menu.upload_method.swdMethod.upload.tool=stm32CubeProg

F103CBT6_BluePill.menu.upload_method.serialMethod=STM32CubeProgrammer (Serial)
F103CBT6_BluePill.menu.upload_method.serialMethod.upload.protocol=1
F103CBT6_BluePill.menu.upload_method.serialMethod.upload.options={serial.port.file} -s
F103CBT6_BluePill.menu.upload_method.serialMethod.upload.tool=stm32CubeProg

F103CBT6_BluePill.menu.upload_method.dfuMethod=STM32CubeProgrammer (DFU)
F103CBT6_BluePill.menu.upload_method.dfuMethod.upload.protocol=2
F103CBT6_BluePill.menu.upload_method.dfuMethod.upload.options=-g
F103CBT6_BluePill.menu.upload_method.dfuMethod.upload.tool=stm32CubeProg

F103CBT6_BluePill.menu.upload_method.bmpMethod=BMP (Black Magic Probe)
F103CBT6_BluePill.menu.upload_method.bmpMethod.upload.protocol=gdb_bmp
F103CBT6_BluePill.menu.upload_method.bmpMethod.upload.tool=bmp_upload

F103CBT6_BluePill.menu.upload_method.hidMethod=HID Bootloader 2.2
F103CBT6_BluePill.menu.upload_method.hidMethod.upload.protocol=hid22
F103CBT6_BluePill.menu.upload_method.hidMethod.upload.tool=hid_upload
F103CBT6_BluePill.menu.upload_method.hidMethod.build.flash_offset=0x800
F103CBT6_BluePill.menu.upload_method.hidMethod.build.bootloader_flags=-DBL_HID -DVECT_TAB_OFFSET={build.flash_offset}

F103CBT6_BluePill.menu.upload_method.dfu2Method=Maple DFU Bootloader 2.0
F103CBT6_BluePill.menu.upload_method.dfu2Method.upload.protocol=maple
F103CBT6_BluePill.menu.upload_method.dfu2Method.upload.tool=maple_upload
F103CBT6_BluePill.menu.upload_method.dfu2Method.upload.usbID=1EAF:0003
F103CBT6_BluePill.menu.upload_method.dfu2Method.upload.altID=2
F103CBT6_BluePill.menu.upload_method.dfu2Method.build.flash_offset=0x2000
F103CBT6_BluePill.menu.upload_method.dfu2Method.build.bootloader_flags=-DBL_LEGACY_LEAF -DVECT_TAB_OFFSET={build.flash_offset}

F103CBT6_BluePill.menu.upload_method.dfuoMethod=Maple DFU Bootloader original
F103CBT6_BluePill.menu.upload_method.dfuoMethod.upload.protocol=maple
F103CBT6_BluePill.menu.upload_method.dfuoMethod.upload.tool=maple_upload
F103CBT6_BluePill.menu.upload_method.dfuoMethod.upload.usbID=1EAF:0003
F103CBT6_BluePill.menu.upload_method.dfuoMethod.upload.altID=1
F103CBT6_BluePill.menu.upload_method.dfuoMethod.build.flash_offset=0x5000
F103CBT6_BluePill.menu.upload_method.dfuoMethod.build.bootloader_flags=-DBL_LEGACY_LEAF -DVECT_TAB_OFFSET={build.flash_offset}

# Serialx activation
F103CBT6_BluePill.menu.xserial.generic=Enabled (generic 'Serial')
F103CBT6_BluePill.menu.xserial.none=Enabled (no generic 'Serial')
F103CBT6_BluePill.menu.xserial.none.build.xSerial=-DHAL_UART_MODULE_ENABLED -DHWSERIAL_NONE
F103CBT6_BluePill.menu.xserial.disabled=Disabled (no Serial support)
F103CBT6_BluePill.menu.xserial.disabled.build.xSerial=

# USB connectivity
F103CBT6_BluePill.menu.usb.none=None
F103CBT6_BluePill.menu.usb.CDCgen=CDC (generic 'Serial' supersede U(S)ART)
F103CBT6_BluePill.menu.usb.CDCgen.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC
F103CBT6_BluePill.menu.usb.CDC=CDC (no generic 'Serial')
F103CBT6_BluePill.menu.usb.CDC.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC -DDISABLE_GENERIC_SERIALUSB
F103CBT6_BluePill.menu.usb.HID=HID (keyboard and mouse)
F103CBT6_BluePill.menu.usb.HID.build.enable_usb={build.usb_flags} -DUSBD_USE_HID_COMPOSITE
F103CBT6_BluePill.menu.xusb.FS=Low/Full Speed
F103CBT6_BluePill.menu.xusb.HS=High Speed
F103CBT6_BluePill.menu.xusb.HS.build.usb_speed=-DUSE_USB_HS
F103CBT6_BluePill.menu.xusb.HSFS=High Speed in Full Speed mode
F103CBT6_BluePill.menu.xusb.HSFS.build.usb_speed=-DUSE_USB_HS -DUSE_USB_HS_IN_FS

# Optimizations
F103CBT6_BluePill.menu.opt.osstd=Smallest (-Os default)
F103CBT6_BluePill.menu.opt.oslto=Smallest (-Os) with LTO
F103CBT6_BluePill.menu.opt.oslto.build.flags.optimize=-Os -flto
F103CBT6_BluePill.menu.opt.o1std=Fast (-O1)
F103CBT6_BluePill.menu.opt.o1std.build.flags.optimize=-O1
F103CBT6_BluePill.menu.opt.o1lto=Fast (-O1) with LTO
F103CBT6_BluePill.menu.opt.o1lto.build.flags.optimize=-O1 -flto
F103CBT6_BluePill.menu.opt.o2std=Faster (-O2)
F103CBT6_BluePill.menu.opt.o2std.build.flags.optimize=-O2
F103CBT6_BluePill.menu.opt.o2lto=Faster (-O2) with LTO
F103CBT6_BluePill.menu.opt.o2lto.build.flags.optimize=-O2 -flto
F103CBT6_BluePill.menu.opt.o3std=Fastest (-O3)
F103CBT6_BluePill.menu.opt.o3std.build.flags.optimize=-O3
F103CBT6_BluePill.menu.opt.o3lto=Fastest (-O3) with LTO
F103CBT6_BluePill.menu.opt.o3lto.build.flags.optimize=-O3 -flto
F103CBT6_BluePill.menu.opt.ogstd=Debug (-Og)
F103CBT6_BluePill.menu.opt.ogstd.build.flags.optimize=-Og

# Debug information
F103CBT6_BluePill.menu.dbg.none=None
F103CBT6_BluePill.menu.dbg.enable=Enabled (-g)
F103CBT6_BluePill.menu.dbg.enable.build.flags.debug=-g

# C Runtime Library
F103CBT6_BluePill.menu.rtlib.nano=Newlib Nano (default)
F103CBT6_BluePill.menu.rtlib.nanofp=Newlib Nano + Float Printf
F103CBT6_BluePill.menu.rtlib.nanofp.build.flags.ldspecs=--specs=nano.specs -u _printf_float
F103CBT6_BluePill.menu.rtlib.nanofs=Newlib Nano + Float Scanf
F103CBT6_BluePill.menu.rtlib.nanofs.build.flags.ldspecs=--specs=nano.specs -u _scanf_float
F103CBT6_BluePill.menu.rtlib.nanofps=Newlib Nano + Float Printf/Scanf
F103CBT6_BluePill.menu.rtlib.nanofps.build.flags.ldspecs=--specs=nano.specs -u _printf_float -u _scanf_float
F103CBT6_BluePill.menu.rtlib.full=Newlib Standard
F103CBT6_BluePill.menu.rtlib.full.build.flags.ldspecs=


################################################################################
# J-Tagit F4

J-TagitF4.name=J-Tagit STM32F4 series

J-TagitF4.build.core=arduino
J-TagitF4.build.board=J-TagitF4
J-TagitF4.build.extra_flags=-D{build.product_line} {build.enable_usb} {build.xSerial} {build.bootloader_flags}
J-TagitF4.build.mcu=cortex-m4
J-TagitF4.build.flags.fp=-mfpu=fpv4-sp-d16 -mfloat-abi=hard
J-TagitF4.build.series=STM32F4xx
J-TagitF4.build.cmsis_lib_gcc=arm_cortexM4lf_math


# J-Tagit F411CEU6
J-TagitF4.menu.pnum.J-Tagit_F411CEUX=J-Tagit F411CEU6
J-TagitF4.menu.pnum.J-Tagit_F411CEUX.upload.maximum_size=524288
J-TagitF4.menu.pnum.J-Tagit_F411CEUX.upload.maximum_data_size=131072
J-TagitF4.menu.pnum.J-Tagit_F411CEUX.build.board=J-Tagit_F411CEU6
J-TagitF4.menu.pnum.J-Tagit_F411CEUX.build.product_line=STM32F411xE
J-TagitF4.menu.pnum.J-Tagit_F411CEUX.build.variant=STM32F4xx/F411CEU6

# J-Tagit F411CCY6
J-TagitF4.menu.pnum.J-Tagit_F411CCYX=J-Tagit F411CCYx
J-TagitF4.menu.pnum.J-Tagit_F411CCYX.upload.maximum_size=262144
J-TagitF4.menu.pnum.J-Tagit_F411CCYX.upload.maximum_data_size=131072
J-TagitF4.menu.pnum.J-Tagit_F411CCYX.build.board=J-Tagit_F411CCYX
J-TagitF4.menu.pnum.J-Tagit_F411CCYX.build.product_line=STM32F411xE
J-TagitF4.menu.pnum.J-Tagit_F411CCYX.build.variant=STM32F4xx/F411C(C-E)(U-Y)


# J-Tagit F427ZITx
J-TagitF4.menu.pnum.J-Tagit_F427ZITX=J-Tagit F427ZITx
J-TagitF4.menu.pnum.J-Tagit_F427ZITX.upload.maximum_size=2097152
J-TagitF4.menu.pnum.J-Tagit_F427ZITX.upload.maximum_data_size=196608
J-TagitF4.menu.pnum.J-Tagit_F427ZITX.build.board=J-Tagit_F427ZITX
J-TagitF4.menu.pnum.J-Tagit_F427ZITX.build.product_line=STM32F427xx
J-TagitF4.menu.pnum.J-Tagit_F427ZITX.build.variant=STM32F4xx/F427Z(G-I)T_F429ZET_F429Z(G-I)(T-Y)_F437Z(G-I)T_F439Z(G-I)(T-Y)

# J-Tagit F429ZITx
J-TagitF4.menu.pnum.J-Tagit_F429ZITX=J-Tagit F429ZITx
J-TagitF4.menu.pnum.J-Tagit_F429ZITX.upload.maximum_size=2097152
J-TagitF4.menu.pnum.J-Tagit_F429ZITX.upload.maximum_data_size=196608
J-TagitF4.menu.pnum.J-Tagit_F429ZITX.build.board=J-Tagit_F429ZITX
J-TagitF4.menu.pnum.J-Tagit_F429ZITX.build.product_line=STM32F429xx
J-TagitF4.menu.pnum.J-Tagit_F429ZITX.build.variant=STM32F4xx/F427Z(G-I)T_F429ZET_F429Z(G-I)(T-Y)_F437Z(G-I)T_F439Z(G-I)(T-Y)

# Upload menu
J-TagitF4.menu.upload_method.swdMethod=STM32CubeProgrammer (SWD)
J-TagitF4.menu.upload_method.swdMethod.upload.protocol=0
J-TagitF4.menu.upload_method.swdMethod.upload.options=-g
J-TagitF4.menu.upload_method.swdMethod.upload.tool=stm32CubeProg

J-TagitF4.menu.upload_method.serialMethod=STM32CubeProgrammer (Serial)
J-TagitF4.menu.upload_method.serialMethod.upload.protocol=1
J-TagitF4.menu.upload_method.serialMethod.upload.options={serial.port.file} -s
J-TagitF4.menu.upload_method.serialMethod.upload.tool=stm32CubeProg

J-TagitF4.menu.upload_method.dfuMethod=STM32CubeProgrammer (DFU)
J-TagitF4.menu.upload_method.dfuMethod.upload.protocol=2
J-TagitF4.menu.upload_method.dfuMethod.upload.options=-g
J-TagitF4.menu.upload_method.dfuMethod.upload.tool=stm32CubeProg

J-TagitF4.menu.upload_method.bmpMethod=BMP (Black Magic Probe)
J-TagitF4.menu.upload_method.bmpMethod.upload.protocol=gdb_bmp
J-TagitF4.menu.upload_method.bmpMethod.upload.tool=bmp_upload

J-TagitF4.menu.upload_method.hidMethod=HID Bootloader 2.2
J-TagitF4.menu.upload_method.hidMethod.upload.protocol=hid22
J-TagitF4.menu.upload_method.hidMethod.upload.tool=hid_upload
J-TagitF4.menu.upload_method.hidMethod.build.flash_offset=0x4000
J-TagitF4.menu.upload_method.hidMethod.build.bootloader_flags=-DBL_HID -DVECT_TAB_OFFSET={build.flash_offset}


################################################################################
# J-Tagit L1
J-TagitL1.name=J-Tagit STM32L1 series

J-TagitL1.build.core=arduino
J-TagitL1.build.board=J-TagitL1
J-TagitL1.build.extra_flags=-D{build.product_line} {build.enable_usb} {build.xSerial}
J-TagitL1.build.mcu=cortex-m3
J-TagitL1.build.series=STM32L1xx
J-TagitL1.build.cmsis_lib_gcc=arm_cortexM3l_math

# J-Tagit L152RETx
J-TagitL1.menu.pnum.J-Tagit_L152RETX=J-Tagit L152RETx
J-TagitL1.menu.pnum.J-Tagit_L152RETX.upload.maximum_size=524288
J-TagitL1.menu.pnum.J-Tagit_L152RETX.upload.maximum_data_size=81920
J-TagitL1.menu.pnum.J-Tagit_L152RETX.build.board=J-Tagit_L152RETX
J-TagitL1.menu.pnum.J-Tagit_L152RETX.build.product_line=STM32L152xE
J-TagitL1.menu.pnum.J-Tagit_L152RETX.build.variant=STM32L1xx/L151RET_L152RET_L162RET

# Upload menu
J-TagitL1.menu.upload_method.swdMethod=STM32CubeProgrammer (SWD)
J-TagitL1.menu.upload_method.swdMethod.upload.protocol=0
J-TagitL1.menu.upload_method.swdMethod.upload.options=-g
J-TagitL1.menu.upload_method.swdMethod.upload.tool=stm32CubeProg

J-TagitL1.menu.upload_method.serialMethod=STM32CubeProgrammer (Serial)
J-TagitL1.menu.upload_method.serialMethod.upload.protocol=1
J-TagitL1.menu.upload_method.serialMethod.upload.options={serial.port.file} -s
J-TagitL1.menu.upload_method.serialMethod.upload.tool=stm32CubeProg

J-TagitL1.menu.upload_method.dfuMethod=STM32CubeProgrammer (DFU)
J-TagitL1.menu.upload_method.dfuMethod.upload.protocol=2
J-TagitL1.menu.upload_method.dfuMethod.upload.options=-g
J-TagitL1.menu.upload_method.dfuMethod.upload.tool=stm32CubeProg

################################################################################
################################################################################
################################################################################
# Serialx activation

J-TagitF4.menu.xserial.generic=Enabled (generic 'Serial')
J-TagitF4.menu.xserial.none=Enabled (no generic 'Serial')
J-TagitF4.menu.xserial.none.build.xSerial=-DHAL_UART_MODULE_ENABLED -DHWSERIAL_NONE
J-TagitF4.menu.xserial.disabled=Disabled (no Serial support)
J-TagitF4.menu.xserial.disabled.build.xSerial=

J-TagitL1.menu.xserial.generic=Enabled (generic 'Serial')
J-TagitL1.menu.xserial.none=Enabled (no generic 'Serial')
J-TagitL1.menu.xserial.none.build.xSerial=-DHAL_UART_MODULE_ENABLED -DHWSERIAL_NONE
J-TagitL1.menu.xserial.disabled=Disabled (no Serial support)
J-TagitL1.menu.xserial.disabled.build.xSerial=

# USB connectivity

J-TagitF4.menu.usb.none=None
J-TagitF4.menu.usb.CDCgen=CDC (generic 'Serial' supersede U(S)ART)
J-TagitF4.menu.usb.CDCgen.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC
J-TagitF4.menu.usb.CDC=CDC (no generic 'Serial')
J-TagitF4.menu.usb.CDC.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC -DDISABLE_GENERIC_SERIALUSB
J-TagitF4.menu.usb.HID=HID (keyboard and mouse)
J-TagitF4.menu.usb.HID.build.enable_usb={build.usb_flags} -DUSBD_USE_HID_COMPOSITE
J-TagitF4.menu.xusb.FS=Low/Full Speed
J-TagitF4.menu.xusb.HS=High Speed
J-TagitF4.menu.xusb.HS.build.usb_speed=-DUSE_USB_HS
J-TagitF4.menu.xusb.HSFS=High Speed in Full Speed mode
J-TagitF4.menu.xusb.HSFS.build.usb_speed=-DUSE_USB_HS -DUSE_USB_HS_IN_FS

J-TagitL1.menu.usb.none=None
J-TagitL1.menu.usb.CDCgen=CDC (generic 'Serial' supersede U(S)ART)
J-TagitL1.menu.usb.CDCgen.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC
J-TagitL1.menu.usb.CDC=CDC (no generic 'Serial')
J-TagitL1.menu.usb.CDC.build.enable_usb={build.usb_flags} -DUSBD_USE_CDC -DDISABLE_GENERIC_SERIALUSB
J-TagitL1.menu.usb.HID=HID (keyboard and mouse)
J-TagitL1.menu.usb.HID.build.enable_usb={build.usb_flags} -DUSBD_USE_HID_COMPOSITE

# Optimizations

J-TagitF4.menu.opt.osstd=Smallest (-Os default)
J-TagitF4.menu.opt.oslto=Smallest (-Os) with LTO
J-TagitF4.menu.opt.oslto.build.flags.optimize=-Os -flto
J-TagitF4.menu.opt.o1std=Fast (-O1)
J-TagitF4.menu.opt.o1std.build.flags.optimize=-O1
J-TagitF4.menu.opt.o1lto=Fast (-O1) with LTO
J-TagitF4.menu.opt.o1lto.build.flags.optimize=-O1 -flto
J-TagitF4.menu.opt.o2std=Faster (-O2)
J-TagitF4.menu.opt.o2std.build.flags.optimize=-O2
J-TagitF4.menu.opt.o2lto=Faster (-O2) with LTO
J-TagitF4.menu.opt.o2lto.build.flags.optimize=-O2 -flto
J-TagitF4.menu.opt.o3std=Fastest (-O3)
J-TagitF4.menu.opt.o3std.build.flags.optimize=-O3
J-TagitF4.menu.opt.o3lto=Fastest (-O3) with LTO
J-TagitF4.menu.opt.o3lto.build.flags.optimize=-O3 -flto
J-TagitF4.menu.opt.ogstd=Debug (-Og)
J-TagitF4.menu.opt.ogstd.build.flags.optimize=-Og

J-TagitL1.menu.opt.osstd=Smallest (-Os default)
J-TagitL1.menu.opt.oslto=Smallest (-Os) with LTO
J-TagitL1.menu.opt.oslto.build.flags.optimize=-Os -flto
J-TagitL1.menu.opt.o1std=Fast (-O1)
J-TagitL1.menu.opt.o1std.build.flags.optimize=-O1
J-TagitL1.menu.opt.o1lto=Fast (-O1) with LTO
J-TagitL1.menu.opt.o1lto.build.flags.optimize=-O1 -flto

# Debug information

J-TagitF4.menu.dbg.none=None
J-TagitF4.menu.dbg.enable=Enabled (-g)
J-TagitF4.menu.dbg.enable.build.flags.debug=-g

# C Runtime Library

J-TagitF4.menu.rtlib.nano=Newlib Nano (default)
J-TagitF4.menu.rtlib.nanofp=Newlib Nano + Float Printf
J-TagitF4.menu.rtlib.nanofp.build.flags.ldspecs=--specs=nano.specs -u _printf_float
J-TagitF4.menu.rtlib.nanofs=Newlib Nano + Float Scanf
J-TagitF4.menu.rtlib.nanofs.build.flags.ldspecs=--specs=nano.specs -u _scanf_float
J-TagitF4.menu.rtlib.nanofps=Newlib Nano + Float Printf/Scanf
J-TagitF4.menu.rtlib.nanofps.build.flags.ldspecs=--specs=nano.specs -u _printf_float -u _scanf_float
J-TagitF4.menu.rtlib.full=Newlib Standard
J-TagitF4.menu.rtlib.full.build.flags.ldspecs=




